
/**
 * Décrivez votre classe Main ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Main
{

    public static void main(){
        Counter counter = new Counter(10_000); // An instance of Counter class to add 10 000
        CounterStarter cs1 = new CounterStarter(counter); // One thread to start count
        CounterStarter cs2 = new CounterStarter(counter); // Another thread to start count
        // Starting threads
        cs1.start();
        cs2.start();
        try { // Waiting for threads' end
        cs1.join(); // Increase counter.value by 10 000
        cs2.join(); // Increase counter.value by 10 000
        System.out.println("Total : " + counter.value); // NOT 20 000 !
        } catch (Exception e) {
        e.printStackTrace();
        }     
    }
}
